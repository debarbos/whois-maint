from yattag import Doc, indent
import datetime

# global yattag vars
doc, tag, text, line  = Doc().ttl()

# YAML layout:
    # subsystems: list of subsystem dicts
        # subsystem: dicts with nested dicts or lists as value pairs
            # labels: dict {name, readyForMergeDeps, newLabels, emailLabel}
            # status: value
            # devel-sst: list of values
            # requiredApproval: value
            # maintainers: list of dicts (can have duplicate names but
            # multiple emails)
                # name: string 
                # email: string
                # gluser: string
                # restricted: T/F
                # field is optional, defaults to False
            # paths: dict {includes, includeRegexes}
            # scm: value
            # mailingList: value

# A helper for formatting each subsystem
def fmtSubsystem(subsystem, maintainer):
    if subsystem.get('paths', None) is not None:
        paths = subsystem['paths']['includes']
    else:
        paths = ['NONE']

    doc.asis(f'''<td><details>
    <summary>{subsystem["subsystem"]}</summary>
    <p> devel-sst: {subsystem["devel-sst"]} </p>
    <p> paths: {paths} </p>
    </details>
    </td>''')
    #line('td',str(subsystem['subsystem']))
    line('td',str(subsystem['status']))
    line('td',str(maintainer['name']))
    line('td',str(maintainer['email']))

# Passing the dict data to this function
# Here, we iterate over every item in the list, and programatically make
# a static HTML file which demonstrates this
def makeHTML(data):


    with tag('html'):
        with tag('head'):
            with tag('title'):
                text('Whois Maint?')
            doc.asis('<link rel="stylesheet" type="text/css" href="style.css">')

        with tag('body', id = 'maintinfo'):
            with tag('h1'):
                text('Whois Kernel Maintainer?')

            with tag('p'):
                line('p','For questions/concerns/comments, message #team-kernel-maint on Slack.')
                line('p','Or, send an email to the kernel maintainers SST.')

            with tag('p'):
                with tag('a', href ='//gitlab.com/redhat/centos-stream/src/kernel/documentation'
                        ):
                    text('Alternatively, submit an MR @ gitlab.com/redhat/centos-stream/src/kernel/documentation')

            with tag('p'):
                with tag('a', href ='//gitlab.com/debarbos/whois-maint'
                        ):
                    text('Source Code for this webpage')

            with tag('table'):
                with tag('tr'):
                    line('th','Subsystem')
                    line('th','Status')
                    line('th','Maintainer')
                    line('th','E-Mail')

                for item in data['subsystems']:
                    with tag('tr'):
                        firstIter = True
                        for element in item['maintainers']:
                            # Check if we need to make a new row for this
                            # "duplicate" item
                            if firstIter:
                                fmtSubsystem(item, element)
                                firstIter = False
                            else:
                                with tag('tr'):
                                    fmtSubsystem(item, element)

            with tag('p', style = "color: white"):
                text("_")

        with tag('footer'):
            time = datetime.datetime.now(datetime.timezone.utc)
            text(' maintained by debarbos(AT)redhat(dot)com  ')
            text(' last nightly update ' + str(time.strftime("%m/%d")) + ' (UTC) : ')
            text(str(time.strftime("%H:%M:%S")))

    return(indent(doc.getvalue()))

