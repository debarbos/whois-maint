from parser import parseData, getURL
from generator import makeHTML

# This file will be the main entry point for the generator
# Point the URL variable to the current location of owners.yaml at C9S Main
URL = "https://gitlab.com/redhat/centos-stream/src/kernel/documentation/-/raw/main/info/owners.yaml?ref_type=heads"

def main():
    yaml = parseData(getURL(URL))
    html = makeHTML(yaml)

    file = open('./public/index.html', 'w')
    file.write(html)
    file.close

if __name__ == "__main__":
    main()
