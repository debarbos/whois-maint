import sys
import requests
import yaml

# GET raw YAML, validate, decode, return.
def getURL(URL):
    ret = requests.get(URL)

    # Decode the data *only* if request is valid
    data = ret.content.decode("utf-8") if ret.ok else sys.exit("Unsuccesful GET")

    return data

# "parse" the raw decoded YAML data into a dict object. 
def parseData(data):

    # Load the YAML as a dictionary of values.
    return yaml.safe_load(data)
