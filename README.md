<div align="center">
<h1> whois-maint </h1>
<p>
  whois-maint is intended to make it easier to see "who is maintaining X"
  at-a-glance.

  Currently, people rely on memory, RHMaintainers, owners.yaml, word-of-mouth,
  lunar calendars, and abacuses to recall the maintainer of a specific release
  or stream.

  Managers, stakeholders, and other product-owners rely on spreadsheets, gdocs,
  and information compiled from the aforementioned sources, and therefore have to 
  manually update these scattered documents to remove the possibility of 
  out-of-date or plain erroneuos information.

  <strong> Why rely on human error? </strong>

  whois-maint automates all of the above. just navigate to <a
  href="https://debarbos.gitlab.io/whois-maint/"> whois-maint </a>
</p>
</div>


<!-- Table of Contents -->
# :notebook_with_decorative_cover: Table of Contents

- [:notebook\_with\_decorative\_cover: Table of Contents](#notebook_with_decorative_cover-table-of-contents)
  - [:smile: Getting Started](#smile-getting-started)
    - [:bangbang: Prerequisites](#bangbang-prerequisites)
    - [:runner: Run Locally](#runner-run-locally)
  - [:dvd: Installation](#dvd-installation)
  - [:computer: Usage](#computer-usage)
  - [:pencil2: License](#pencil2-license)
  - [:handshake: Contact](#handshake-contact)


<!-- Getting Started -->
## :smile: Getting Started

<!-- Prerequisites -->
### :bangbang: Prerequisites

This project uses pip as a package manager

Ensure your version of pip is up-to-date.

Tested/Developed on Python 3.11

```bash
    python -m pip install -–upgrade pip
```

<!-- Run Locally -->
### :runner: Run Locally

Clone the project via HTTPS:

```bash
    git clone https://gitlab.com/debarbos/whois-maint.git
```

Alternatively, clone it via SSH:

```bash
    git clone git@gitlab.com:debarbos/whois-maint.git
```

<!-- Installation -->
## :dvd: Installation

To install into your local user environment from a checked-out source tree

```
pip install .
```

We recommend a separate virtualenv, particularly if you wish to have
an editable install for iterative development.

``` bash
$ python3 -m venv venv
$ ./venv/bin/pip install .  # use "-e" for a development install
```

<!-- Usage -->
## :computer: Usage

From the root of the repository (and with your venv active):

```bash
$ python ./src/whois-maint/main.py
```

A new `html.index` file will be generated in the `public/` directory. 

Interpret the file however you please.

Note that the current state of the 'public/' directory DOES NOT reflect what is
actively deployed. Please check the pipeline jobs for an accurate reflection of
"served" files.

<!-- License -->
## :pencil2: License

Distributed under the Apache 2.0 License. Pease See [LICENSE.md](./LICENSE.md) for more information.


<!-- Contact -->
## :handshake: Contact

Derek Barbosa - debarbos (AT) redhat (DOT) com
